## multipart
一个基于[multiparty](https://github.com/pillarjs/multiparty "multiparty")实现的 `koa2` 的解析 `multipart/form-data` (文件上传)的内容解析器。
### 安装
```javascript
npm install koa-multipart
```
### 使用
```javascript
let Koa = require('koa');
let multipart = require('koa-multipart');

let app = new Koa();
let.use(multipart());

let.use(async (ctx) => {
  // 解析后的请求内容将存放到 ctx.request.body 上
  // 如果没有解析到对应的请求，则 body 将是 undefined
  ctx.body = ctx.request.body;
});
```
每一次解析到文件时，都会将文件保存到 `uploadDir` 配置的路径上。解析完成后，会将解析到的文件和字段数据放入 `ctx.request.body`，格式如下：
```javascript
ctx.request.body = {
  files: [{……}],
  fields: {}
}
```
* **files**: 所有的文件信息 **file**, **file** 是一个对象，存放的是文件的基本信息：
  * **fieldName**: 上传文件时传递的 name 字段
  * **originalName**： 上传的文件名
  * **path**: 上传后文件保存到服务器时的路径
* **fields**: 解析到的所有的普通字段数据
### 参数说明
```javascript
multipart(opts)
```
* **uploadDir**: `function(obj)`，返回文件的保存路径；默认为
  ```
  path.join('/', md5(filenamePre + Date.now()) + suffix)
  ```
  参数**obj**是一个对象，包含有以下字段：
  * **name**: 上传文件的 `name` 字段
  * **filename**: 上传的文件名称, 例如：`login.jpg`
  * **filenamePre**: 文件名称(不带后缀)，例如：`login`
  * **suffix**: 文件后缀，例如：`.jpg`

