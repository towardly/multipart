/**!
 * koa-multipart - test/index.test.js
 * Copyright(c) 2018 Tenny
 * MIT Licensed
 */
const koa = require('koa');
const multiparty = require('..');
const request = require('supertest');
const { expect } = require('chai');
const path = require('path');

describe('multipart/form-data', () => {

  describe('test url', () => {

    let app = App({
      uploadDir: function(file) {
        return path.join('/', file.filenamePre + 1 + file.suffix);
      }
    });
    app.use(async (ctx) => {
      ctx.body = 'success';
    });
    it('url', function(done) {
      request(app.listen())
        .post('/request')
        .field('name', 'my awesome avatar')
        .attach('file', '/login.png')
        .expect('success', done);
    });
  })
});

function App(opts) {
  let app = new koa();
  app.use(multiparty(opts));
  return app;
}